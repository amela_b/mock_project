<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        DB::table('roles')->insert([
            [
                "name" => "Admin",
            ],
            [
                "name" => "Quản lý đơn hàng",
            ],
            [
                "name" => "Quản lý sản phẩm",
            ],
            [
                "name" => "Quản lý bài viết",
            ],
            [
                "name" => "Khách hàng",
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
};
