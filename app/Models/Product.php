<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Image;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'code',
        'description',
        'image',
        'category_id',
        'quantity',
        'price',
        'is_hot',
        'is_sale',
        'percent_sale',
        'status',
        'isFreeShip',
        'rating',
        'numberSell',
        'colors',
        'sizes',
        'price_promotion',
        'created_at'
    ];
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function images()
    {
        return $this->hasMany(Image_Product::class);
    }
    public function carts()
    {
        return $this->hasMany(Cart::class);
    }
}
