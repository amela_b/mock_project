<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Image_Product extends Model
{
    use HasFactory;

    protected $table = 'image_product';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_id',
        'image',
        'created_at'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
