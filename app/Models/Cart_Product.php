<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\Cart;

class Cart_Product extends Model
{
    use HasFactory;
    protected $table = 'carts_orders';
    protected $primaryKey = 'id';
    protected $fillable = [
        'cart_id',
        'product_id',
        'created_at'
    ];
   
    // public function carts()
    // {
    //     return $this->belongsToMany(Cart::class, 'cart_id');
    // }
    // public function products()
    // {
    //     return $this->belongsToMany(Product::class, 'product_id');
    // }
}
