<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Cart extends Model
{
    use HasFactory;
    protected $table = 'carts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'c',
        'color',
        'quantity',
        'size',
        'status',
        'user_id',
        'id_product',
        'created_at'
    ];
    public function product()
    {
        return $this->belongsTo(Product::class,'id_product');
    }
}
