<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use JWTAuth;
use App\Model\User;
trait GetAuthenticatedUser
{

    /**
     * Parse response format
     *
     * @param  array $data
     * @param  string $statusCode
     * @return JsonResponse
     */
    public function getAuthenticatedUser()
    {
      
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return null;
        }
        $user = JWTAuth::parseToken()->authenticate();
        return $user;
    }
}
