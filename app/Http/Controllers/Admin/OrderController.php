<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\StoreOrderRequest;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\Product_Order;
use App\Services\OrderService;
use DB;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $service;
    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }
    public function index(Request $request)
    {
        $where = [];
        if ($request->name) {
            $where[] = ['name', 'like', '%' . $request->name . '%'];
        }
        if ($request->phone) {
            $phone = $request->input('phone');
            $where[] = ['phone', 'like', '%' . $request->phone . '%'];
        }
        if ($request->type_buy) {
            $where[] = ['type_buy', $request->type_buy];
        }
        if ($request->from_date) {
            $where[] = ['created_at', '>=', $request->from_date];
        }
        if ($request->to_date) {
            $to_date = $request->input('to_date');
            $where[] = ['created_at', '<=', $to_date];
        }
        $listOrder = Order::where($where)->with('user', 'product_order.product');
        if ($request->category_id) {
            $category_id = $request->category_id;
            $listOrderDetail = Product_Order::select('p.name', 'products_orders.order_id')
                ->leftjoin('products as p', 'p.id', 'products_orders.product_id')
                ->where('p.category_id', $category_id)->distinct('products_orders.order_id')->get();
            $arr = [];
            foreach ($listOrderDetail as $ls) {
                $arr[] = $ls->order_id;
            }
            $listOrder = $listOrder->whereIN('orders.id', $arr);
        }
        $total_money = $listOrder->sum('orders.total_money');
        $listOrder = $listOrder->orderby('orders.id', 'desc')->paginate(10);
        $category = Category::all();
        return view('admin.order.index', compact('listOrder', 'category', 'total_money'));
    }

    public function create()
    {
        $category = Category::all();
        $product = Product::all();
        return view('admin.order.create', compact('category', 'product'));
    }

    public function update($id)
    {
        $order = Order::find($id)->load('user', 'product_order.product');
        $category = Category::all();
        $product = Product::all();
        return view('admin.order.update', compact('category', 'order', 'product'));
    }

    public function store(StoreOrderRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->service->createOrder($request);
            DB::commit();
            $url = redirect()->route('order.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }

    public function edit(StoreOrderRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->service->editOrder($request, $id);
            DB::commit();
            $url = redirect()->route('order.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }

    public function delete(Request $request, $id)
    {
        try {
            $delete = Order::where('id', $id)->update([
                'delivery_status' => 4,
            ]);

            $request->session()->flash('success', 'Hủy đơn hàng thành công!');
            $url = redirect()->route('order.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }
}
