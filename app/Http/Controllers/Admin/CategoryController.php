<?php

namespace App\Http\Controllers\Admin;

use App\Traits\UploadImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
   
    public function index(Request $request)
    {
        $category = Category::where('status', 1);
        if (!empty($request->input('name'))) {
            $name = $request->input('name');
            $category = $category->where('name', 'like', '%' . $name . '%');
        }
        $category = $category->paginate(6);
        return view('admin.category.index', compact('category'));
    }
   
    use UploadImage;
    
    public function store(CategoryRequest $request)
    {
        try {

            $req = $request->all();
            if ($request->file('image') != null) {
                $req['image'] = $this->uploadImage($request->file('image'));
            }
            $data = Category::create($req);
            $request->session()->flash('success', 'Thêm mới danh mục thành công!');
            return redirect()->back();

        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }
    
    public function edit(CategoryRequest $request, $id)
    {
        try {

            $req = $request->all();
            if ($request->file('image') != null) {
                $req['image'] = $this->uploadImage($request->file('image'));
            }
            $data = Category::findorfail($id);
            $data->update($req);
            $request->session()->flash('success', 'Sửa danh mục thành công!');
            return redirect()->back();
        } catch (\Throwable$th) {
            $request->session()->flash('error', 'Vui lòng thử lại sau!');
            return back();
        }
    }

    

    public function delete(Request $request, $id)
    {
        $delete = Category::where('id', $id)->update([
            'status' => 0,
        ]);
        $data = Product::where('category_id', $id)->update([
            'status' => 0,
        ]);
        $request->session()->flash('success', 'Xóa danh mục thành công!');
        return redirect()->back();
    }

}
