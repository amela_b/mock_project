<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use DB;

class DataDeleteController extends Controller
{
    public function index(Request $request)
    {
    
        return view('admin.data_deleted.index');
    }
    public function deletedOrder(Request $request)
    {
        $listOrder = Order::where('delivery_status',4)->with('user')->get();
       

        return view('admin.data_deleted.deleted_order', compact('listOrder'));
    }
    public function deletedProduct(Request $request)
    {
        $listProduct = Product::where('status',0)->get();
    
        return view('admin.data_deleted.deleted_product', compact('listProduct'));
    }
    public function deletedCategory(Request $request)
    {
        $listCategory = Category::where('status',0)->get();
    
        return view('admin.data_deleted.deleted_category', compact('listCategory'));
    }
    public function restore(Request $request)
    {
        $id = $request->id;
        
        $data = Order::whereIn('id',$id)->update([
            'delivery_status' => 1,
        ]);
        $request->session()->flash('success', 'Khôi phục đơn hàng thành công!');
        $url = redirect()->route('delete.order')->getTargetUrl();
        return redirect($url);
       
        
    }
    
    public function restoreProduct(Request $request)
    {
        $id = $request->id;
        
        $data = Product::whereIn('id',$id)->update([
            'status' => 1,
        ]);
        $request->session()->flash('success', 'Khôi phục sản phẩm thành công!');
        $url = redirect()->route('delete.order')->getTargetUrl();
        return redirect($url);
       
        
    }
    public function restoreCategory(Request $request)
    {
        $id = $request->id;
        $cate = Category::whereIn('id',$id)->update([
            'status' => 1,
        ]);
        $cate = Product::whereIn('category_id',$id)->update([
            'status' => 1,
        ]);
        $request->session()->flash('success', 'Khôi phục danh mục thành công!');
        $url = redirect()->route('delete.order')->getTargetUrl();
        return redirect($url);
       
        
    }
    
}
