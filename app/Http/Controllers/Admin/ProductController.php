<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\EditProductRequest;
use App\Models\Category;
use App\Models\Image_Product;
use App\Models\Product;
use App\Services\ProductService;
use App\Traits\UploadImage;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $service;
    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }
    use UploadImage;
    public function index(Request $request)
    {
        $listProduct = Product::select('products.*', 'c.name as category_name')
            ->leftjoin('category as c', 'c.id', 'products.category_id')
            ->where('products.status', 1);
        if (!empty($request->input('name'))) {
            $name = $request->input('name');
            $listProduct = $listProduct->where('products.name', 'like', '%' . $name . '%');
        }
        if (!empty($request->input('is_sale'))) {
            $is_sale = $request->input('is_sale');
            $listProduct = $listProduct->where('products.is_sale', $is_sale);
        }
        if (!empty($request->input('is_hot'))) {
            $is_hot = $request->input('is_hot');
            $listProduct = $listProduct->where('products.is_hot', $is_hot);
        }
        if (!empty($request->input('category_id'))) {
            $category_id = $request->input('category_id');
            $listProduct = $listProduct->where('products.is_hot', $category_id);
        }
        if (!empty($request->input('from_date'))) {
            $from_date = $request->input('from_date');
            $listProduct = $listProduct->where('products.created_at', '>=', $from_date);
        }
        if (!empty($request->input('to_date'))) {
            $to_date = $request->input('to_date');
            $listProduct = $listProduct->where('products.created_at', '<=', date('Y-m-d H:i:s', strtotime($to_date) + 24 * 3600 - 1));
        }
        $listProduct = $listProduct->orderby('products.id', 'desc')->paginate(6);
        $category = Category::all();
        return view('admin.product.index', compact('listProduct', 'category'));
    }

    public function create()
    {
        $category = Category::all();
        return view('admin.product.create', compact('category'));
    }

    public function update($id)
    {
        $product = Product::findorfail($id);
        $image = Image_Product::where('product_id', $id)->get();
        $category = Category::all();
        return view('admin.product.update', compact('category', 'product', 'image'));
    }

    public function store(CreateProductRequest $request)
    {
        DB::beginTransaction();
        try {
            $this->service->createProduct($request);
            DB::commit();
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }

    }
    public function edit(EditProductRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->service->editProduct($request, $id);
            DB::commit();
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }

    }

    public function delete(Request $request, $id)
    {
        try {
            $delete = Product::where('id', $id)->update([
                'status' => 0,
            ]);
            $request->session()->flash('success', 'Xóa sản phẩm thành công!');
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }

    public function update_hot(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->service->updateHot($request, $id);
            DB::commit();
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }
    public function update_sale(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $this->service->updateSale($request, $id);
            DB::commit();
            $url = redirect()->route('product.index')->getTargetUrl();
            return redirect($url);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }

}
