<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Traits\ApiResponse;
use App\Http\Utils\SystemParam;

class CategoryController extends Controller
{
    use ApiResponse;
    public function listCategory(){
        $data = Category::where('status', 1)->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::list_category, $data);
    }

}
