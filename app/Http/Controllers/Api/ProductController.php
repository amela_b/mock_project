<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use DB;
use App\Traits\UploadImage;
use App\Traits\ApiResponse;
use App\Http\Utils\SystemParam;
class ProductController extends Controller
{
    use UploadImage,ApiResponse;
    public function listProduct(Request $request){

        $where = [];
        $orderby =[];
        if ($request->keyword) {
            $where[] = ['name', 'Like', '%' . $request->keyword . '%'];
        }
        if ($request->category) {
            $where[] = ['category_id', $request->category];
        }
        if ($request->price_min) {
            $where[] = ['price_promotion','>=', $request->price_min];
        }
        if ($request->price_max) {
            $where[] = ['price_promotion','<=', $request->price_max];
        }
        if ($request->rating_filter) {
            $where[] = ['rating','>=', $request->rating_filter];
        }

        $limit = $request->limit ?? '12';
      
        $sort_by = $request->sort_by;
       
        $data = DB::table('products')->select('id','name','image','rating','is_sale','numberSell','price','isFreeShip','percent_sale','created_at')->where($where)->where('status','1')->paginate($limit);
        if ($sort_by=='price' && isset($request->order)) {
            $data = Product::where($where)->orderby('price_promotion', $request->order)->paginate($limit);
        }
        if ($sort_by=='ctime') {
            $data = DB::table('products')->select('id','name','image','rating','is_sale','numberSell','price','isFreeShip','percent_sale','created_at')->where($where)->where('status','1')->orderby('created_at', 'DESC')->paginate($limit);
        }
        if ($sort_by=='sales') {
            $data = DB::table('products')->select('id','name','image','rating','is_sale','numberSell','price','isFreeShip','percent_sale','created_at')->where('status','1')->where($where)->orderby('numberSell', 'DESC')->paginate($limit);
        }

        return $this->responseApi(SystemParam::status_success, SystemParam::list_product, $data);
    }
    public function listSellProduct(){
        $data= DB::table('products')->select('id','name', 'image','rating','is_sale','numberSell','price','isFreeShip','percent_sale','created_at')->where([['numberSell','>=','1'],['status','1']])->limit(6)->orderby('numberSell','DESC')->get();
        return $this->responseApi(SystemParam::status_success, SystemParam::list_sell_product, $data);
    }
    public function detailProduct(Request $request, $id){
        $product_detail = Product::findorfail($id);
        $category = Product::findorfail($id)->category;

        $data = array(
            'data' => $product_detail,
            'category' => $category,
        );
        return $this->responseApi(SystemParam::status_success, '', $data);
    }
    public function createProduct(Request $request)
    {
        $req = $request->all();
        
        $req['image'] = $this->uploadArrayImage($request->file('image'));
        
        $req['price_promotion'] = $request->price-($request->price*$request->percent_sale)/100;

        $data = Product::create($req);

        return $this->responseApi(SystemParam::status_success, '', $data);
    }
}
