<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Traits\UploadImage;
use App\Traits\ApiResponse;
use App\Http\Utils\SystemParam;

class BannerController extends Controller
{
    use UploadImage,ApiResponse;
    public function listBanner(){
        $data = Banner::all();
        return $this->responseApi(SystemParam::status_success, '', $data);
    }
    public function createBanner(Request $request){
        $req = $request->all();
            if ($request->file('image')) {
                $req['image'] = $this->uploadImage($request->file('image'));
            }
        $data = Banner::create($req);
        return $this->responseApi(SystemParam::status_success, SystemParam::create_banner, $data);
    }










}
