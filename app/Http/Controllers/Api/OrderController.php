<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\Product_Order;
use App\Notifications\ShopeeNotify;
use App\Traits\ApiResponse;
use App\Traits\GetAuthenticatedUser;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    use GetAuthenticatedUser, ApiResponse;
    public function storeOrder(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
        }
        $id_user = Auth::user()->id;
        $id_cart = explode(",", $request->id_cart);
        $req_order['user_id'] = $id_user;
        $req_order['name'] = $request->name;
        $req_order['phone'] = $request->phone;
        $req_order['address'] = $request->address;
        $req_order['status'] = 1;
        $req_order['type_buy'] = 1;
        $req_order['delivery_status'] = 1;
        $req_order['email'] = Auth::user()->email;
        $req_order['total_money'] = $request->total_money;

        $order = Order::create($req_order);
        $cart = Cart::whereIn('id', $id_cart)->with('product')->get();

        foreach ($cart as $value) {
            $create = Product_Order::insert([
                'order_id' => $order->id,
                'product_id' => $value->id_product,
                'quantity' => $value->quantity,
                'money' => $value->product->price_promotion,
                'size' => $value->size,
                'color' => $value->color,
            ]);
            $sl = $value->product->quantity - $value->quantity;
            Product::where('id', $value->id_product)->update([
                'quantity' => $sl,
            ]);
            $delete = Cart::whereIn('id', $id_cart)->delete();
        }
        $user = $this->getAuthenticatedUser();
        $user->notify(new ShopeeNotify());
        return $this->responseApi(SystemParam::status_success, SystemParam::create_order, $order);

    }
    public function listOrder(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
        }
        $id_user = Auth::user()->id;
        $where = [];
        if ($request->delivery_status) {
            $where[] = ['delivery_status', $request->delivery_status];
        }
        $data = Order::where('user_id', $id_user)->where($where)->with('user', 'product_order.product')->orderby('id', 'DESC')->get();
        return $this->responseApi(SystemParam::status_success, '', $data);
    }
    public function cancelOrder(Request $request, $id)
    {
        try {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
            }
            $data = Order::where('id', $id)->orderby('id', 'DESC')->update([
                'delivery_status' => 4,
            ]);

            return $this->responseApi(SystemParam::status_success, SystemParam::cancel_order, $data);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }
    public function buyAgain(Request $request, $id)
    {
        try {
            $user = $this->getAuthenticatedUser();
            if (!$user) {
                return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
            }
            $order = Order::where('id', $id)->with('product_order.product')->first();
            $order_delivery_status = $order->delivery_status;
            if ($order_delivery_status == 4 || $order_delivery_status == 3) {
                $id_user = Auth::user()->id;

                $req_order['user_id'] = $id_user;
                $req_order['name'] = $order->name;
                $req_order['phone'] = $order->phone;
                $req_order['address'] = $order->address;
                $req_order['status'] = 1;
                $req_order['type_buy'] = 1;
                $req_order['delivery_status'] = 1;
                $req_order['email'] = $order->email;
                $req_order['total_money'] = $order->total_money;

                $order_create = Order::create($req_order);
                $product_order = $order->product_order;
               
                foreach ($product_order as $value) {
                    $create = Product_Order::insert([
                        'order_id' => $order_create->id,
                        'product_id' => $value->product_id,
                        'quantity' => $value->quantity,
                        'money' => $value->money,
                        'size' => $value->size,
                        'color' => $value->color,
                    ]);
                    $sl = $value->product->quantity - $value->quantity;
                    Product::where('id', $value->id_product)->update([
                        'quantity' => $sl,
                    ]);
                }
                if ($order_delivery_status == 4) {
                    $product_order_delete = Product_Order::where('order_id', $id)->delete();
                    $order_delete = Order::where('id', $id)->delete();
                }
                
                
                $user = $this->getAuthenticatedUser();
                $user->notify(new ShopeeNotify());
            } else {
                return $this->responseApi(SystemParam::status_error, SystemParam::buy_again_error, '');
            }

            return $this->responseApi(SystemParam::status_success, SystemParam::buy_again_success, $order_create);
        } catch (\Exception$e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    }
}
