<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponse;
use App\Traits\GetAuthenticatedUser;
use App\Traits\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use UploadImage, GetAuthenticatedUser, ApiResponse;
    public function __construct()
    {
        Auth::setDefaultDriver('api');
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ], [

            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Nhập đúng định dạng email',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => '422',
                'message' => 'Lỗi đăng nhập',
                'data' => [
                    'message' => $validator->getMessageBag()->first(),
                ],

            ], 422);
        }
        $credentials = $request->only('email', 'password');

        $token = Auth::attempt($credentials);
        if (!$token) {
            return response()->json([
                'status' => '401',
                'message' => "Email hoặc password không đúng",
                'data' => [
                    'password' => 'Email hoặc mật khẩu không đúng',
                ],

            ], 401);
        }

        $user = Auth::user();

        return response()->json([
            'status' => '1',
            'message' => 'Đăng nhập thành công!',
            'data' => [
                'user' => $user,
                'access_token' => $token,
                // 'refresh_token' => Auth::refresh(),
                'expires' => 1440,
                'expires_refresh_token' => 20160,
            ],
        ]);

    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ], [

            'email.required' => 'Vui lòng nhập email',
            'email.unique' => 'Email đã tồn tại',
            'email.email' => 'Nhập đúng định dạng email',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'password.min' => "Mật khẩu không dưới :min kí tự!",

        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => '422',
                'message' => 'Lỗi đăng ký',
                'data' => [
                    'email' => $validator->getMessageBag()->first(),
                ],
            ], 422);
        }
        $user = User::create([
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => '',
            'role' => '5',
            'phone' => '',
            'address' => '',
            'avatar' => '',
        ]);

        $token = Auth::login($user);
        $users = Auth::user();
        return response()->json([
            'status' => '1',
            'message' => 'Đăng ký thành viên thành công!',
            'data' => [
                'user' => $users,
                // 'refresh_token' => Auth::refresh(),
                'access_token' => $token,
                'expires' => 1440,
                'expires_refresh_token' => 20160,

            ],
        ]);
    }
    public function getUserInfo(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        return response()->json([
            'status' => '1',
            'message' => '',
            'data' => $user,
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return response()->json([
            'status' => '1',
            'message' => 'Đăng xuất thành công!',
        ]);

    }
    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:160',
            'phone' => 'digits:10|regex:/^0([0-9]{9})+$/',
            'address' => 'string',
        ], [
            'name.string' => 'Tên người dùng phải là kiểu chuỗi',
            'name.max' => 'Tên người dùng không quá :max kí tự',
            'phone.digits' => 'Số điện thoại chỉ gồm 10 số',
            'phone.regex' => 'Số điện thoại chưa đúng định dạng',

        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => '422',
                'message' => 'Lỗi cập nhật',
                'data' => [
                    'message' => $validator->getMessageBag()->first(),
                ],
            ], 422);
        }
        $user = $this->getAuthenticatedUser();

        $req = $request->except('role');
        if ($request->file('avatar') != null) {
            $req['avatar'] = $this->uploadImage($request->file('avatar'));
        }
        $user->update($req);
        return response()->json([
            'status' => '1',
            'message' => 'Cập nhật thành công',
            'data' => [
                'user' => $user,
            ],
        ]);

    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'current_password',
            'new_password' => 'min:6|required_with:confirm_new_password|same:confirm_new_password',
            'confirm_new_password' => 'min:6',
        ], [
            'old_password.current_password' => "Mật khẩu cũ không chính xác!",
            'new_password.required' => "Vui lòng nhập mật khẩu mới!",
            'new_password.min' => "Mật khẩu mới không dưới :min kí tự!",
            'confirm_new_password.confirmed' => "Xác nhận mật khẩu mới không chính xác!",
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => '422',
                'message' => 'Lỗi đăng ký',
                'data' => [
                    'old_password' => $validator->getMessageBag()->first(),
                ],

            ], 422);
        }
        $input = $request->new_password;
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return response()->json([
                'status' => '422',
                'message' => 'Lỗi',
                'data' => [
                    'message' => 'token hết hạn',
                ],
            ], 422);
        }
        $user->update([
            'password' => Hash::make($input),
        ]);
        return response()->json([
            'status' => '1',
            'message' => 'Cập nhật mật khẩu thành công',
            'data' => [
                'user' => $user,
            ],
        ]);
    }

}
