<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Utils\SystemParam;
use App\Models\Cart;
use App\Traits\ApiResponse;
use App\Traits\GetAuthenticatedUser;
use App\Traits\UploadImage;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    use UploadImage, GetAuthenticatedUser, ApiResponse;
    public function createCart(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
        }
        $id_user = Auth::user()->id;
        $listCart = DB::table('carts')->select('color', 'id_product', 'size', 'quantity')->where('status', 1)->get();

        $req = $request->all();
        $req['user_id'] = $id_user;
        $id_product = $request->id_product;
        $color = $request->color;
        $size = $request->size;
        $quantity = $request->quantity;
        if ($listCart->count() > 0) {
            foreach ($listCart as $value) {
                $value_id = $value->id_product;
                $value_color = $value->color;
                $value_size = $value->size;
                $value_quantity = $value->quantity;
                if ($id_product == $value_id && $color == $value_color && $size == $value_size) {
                    $total_quantity = $quantity + $value_quantity;
                    $cart_update = Cart::where([['id_product', $id_product], ['color', $color], ['size', $size]])->update(['quantity' => $total_quantity]);

                    $cart = Cart::where([['id_product', $id_product], ['color', $color], ['size', $size]])->first();
                    return $this->responseApi(SystemParam::status_success, SystemParam::create_cart, $cart);
    
                }
            }
            $cart = Cart::create($req);
        }
        else {
            $cart = Cart::create($req);
        }

        return $this->responseApi(SystemParam::status_success, SystemParam::create_cart, $cart);

    }
    public function editCart(Request $request, $id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
        }
        $req = $request->quantity;
        $cart = Cart::where('id', $id)->update(['quantity' => $req]);
        $data = Cart::findorfail($id);

        return $this->responseApi(SystemParam::status_success, SystemParam::update_cart, $data);
    }
    public function deleteCart(Request $request, $id)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
        }
        $data = Cart::findorfail($id)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::delete_cart, $data);
    }
    public function deleteCartMultiple(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
        }
        $id_cart = explode(",", $request->id_cart);
        $data = Cart::whereIn('id', $id_cart)->delete();
        return $this->responseApi(SystemParam::status_success, SystemParam::delete_cart, $data);
    }

    public function listCart(Request $request)
    {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            return $this->responseApi(SystemParam::status_error, SystemParam::token_expired, null);
        }
        $id_user = Auth::user()->id;

        $data = Cart::where('user_id', $id_user)->with('product')->get();

        return $this->responseApi(SystemParam::status_success, '', $data);
    }
}
