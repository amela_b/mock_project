<?php

namespace App\Http\Requests\Category;
use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' =>'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => "Vui lòng nhập tên danh mục",
           
        ];
    }
}
