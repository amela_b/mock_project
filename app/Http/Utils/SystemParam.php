<?php
namespace App\Http\Utils;

class SystemParam
{
    const status_success = 1;
    const status_error = 0;
    const token_expired = "Token hết hạn, vui lòng đăng nhập";
    const register_success = "Đăng ký thành công";
    const login_success = "Đăng nhập thành công";
    const create_banner = "Tạo banner thành công";
    const list_category = "Danh sách danh mục";
    const list_product = "Danh sách sản phẩm";
    const list_sell_product = "Danh sách sản phẩm bán chạy";
    const create_cart = "Thêm vào giỏ hàng thành công";
    const update_cart = "Cập nhật giỏ hàng thành công";
    const delete_cart = "Xóa giỏ hàng thành công";
    const create_order = "Đặt hàng thành công";
    const cancel_order = "Hủy đơn hàng thành công";
    const buy_again_error = "Lỗi, đơn hàng phải đã mua hoặc đã hủy";
    const buy_again_success = "Mua lại đơn hàng phải thành công";
}
