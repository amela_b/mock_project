<?php

namespace App\Services;

use App\Models\Order;
use App\Models\Product;
use App\Models\Product_Order;
use App\Traits\UploadImage;

class OrderService
{
    use UploadImage;
    public function createOrder($request)
    {
        if (!$request->product_id || count($request->product_id) == 0) {
            $request->session()->flash('error', 'Vui lòng chọn sản phẩm!');
            return redirect()->back();
        }
        $ls_product = Product::whereIn('id', $request->product_id)->get();
        $req_order = $request->all();
        $req_order['total_money'] = $ls_product->sum('price');
        $req_order['status'] = 1;
        $req_order['type_buy'] = 2;
        $req_order['delivery_status'] = 1;

        $order = Order::create($req_order);
        $total_money = 0;
        $price = 0;
        foreach ($ls_product as $ls) {
            $price = $ls->price*$request->quantity;
            if ($ls->is_sale == 1) {
                $price = ($ls->price - $ls->price * $ls->percent_sale / 100)*$request->quantity;
            }
            $total_money += $price;
            $create = Product_Order::insert([
                'order_id' => $order->id,
                'product_id' => $ls->id,
                'quantity' =>  $request->quantity,
                'money' => $price,
            ]);
            Product::where('id', $ls->id)->update([
                'quantity' => $ls->quantity - $request->quantity,
            ]);
        }
        $order->update([
            'total_money' => $total_money,
        ]);
        $request->session()->flash('success', 'Thêm mới đơn hàng thành công!');
    }
    public function editOrder($request, $id)
    {
        
        if ($request->product_id == null) {
            $request->session()->flash('error', 'Vui lòng chọn sản phẩm!');
            return redirect()->back();
        }
        $order = Order::findorfail($id);
        $ls_product = Product::whereIn('id', $request->product_id)->get();
        $req_order = $request->all();
        $req_order['total_money'] = $ls_product->sum('price');
        $order->update($req_order);
        $total_money = 0;
        $price = 0;
        if (count($ls_product) > 0) {
            $delete = Product_Order::where('order_id', $id)->delete();
            foreach ($ls_product as $ls) {
                $price = $ls->price*$request->quantity;
                if ($ls->is_sale == 1) {
                    $price = ($ls->price - $ls->price * $ls->percent_sale / 100)*$request->quantity;
                }
                $total_money += $price;
                $create = Product_Order::insert([
                    'order_id' => $order->id,
                    'product_id' => $ls->id,
                    'quantity' => $request->quantity,
                    'money' => $price,
                ]);
                Product::where('id', $ls->id)->update([
                    'quantity' => $ls->quantity - $request->quantity,
                ]);
            }
        }

        $request->session()->flash('success', 'Cập nhật đơn hàng thành công!');
    }
   
}
