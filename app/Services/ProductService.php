<?php

namespace App\Services;

use App\Models\Product;
use App\Traits\UploadImage;

class ProductService
{
    use UploadImage;
    public function createProduct($request)
    {
        if ($request->input('category_id') == null) {
            $request->session()->flash('error', 'Vui lòng chọn danh mục sản phẩm!');
            return redirect()->back();
        }
        $req = $request->all();
        if ($request->file('image')) {
            $req['image'] = $this->uploadArrayImage($request->file('image'));
        }
        $req['price_promotion'] = $request->price-($request->price*$request->percent_sale)/100;
        $create = Product::create($req);

        $request->session()->flash('success', 'Thêm mới sản phẩm thành công!');
    }
    public function editProduct($request, $id)
    {
        if ($request->category_id == null) {
            $request->session()->flash('error', 'Vui lòng chọn danh mục sản phẩm!');
            return redirect()->back();
        }
        $req = $request->all();

        $product = Product::findorfail($id);
        $req['image'] = $this->uploadArrayImage($request->file('image'));
        $product->update($req);
        $request->session()->flash('success', 'Cập nhật sản phẩm thành công!');
    }
    public function updateHot($request, $id)
    {
        $check = Product::where('id', $id)->first();
        if ($check->is_hot == 1) {
            $update = Product::where('id', $id)->update([
                'is_hot' => 0,
            ]);
            $request->session()->flash('success', 'Tắt HOT sản phẩm thành công!');

        } else {
            $update = Product::where('id', $id)->update([
                'is_hot' => 1,
            ]);
            $request->session()->flash('success', 'Bật HOT sản phẩm thành công!');

        }
    }
    public function updateSale($request, $id)
    {
        $check = Product::where('id', $id)->first();
            if ($check->is_sale == 1) {
                $update = Product::where('id', $id)->update([
                    'is_sale' => 0,
                ]);
                $request->session()->flash('success', 'Tắt Sale sản phẩm thành công!');
               
            } else {
                $update = Product::where('id', $id)->update([
                    'is_sale' => 1,
                ]);
                $request->session()->flash('success', 'Bật Sale sản phẩm thành công!');
                
            }
    }

}
