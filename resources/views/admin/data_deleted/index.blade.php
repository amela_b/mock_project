@extends('admin.layouts.app-new')
@section('title-page', 'Danh sách dữ liệu đã xóa' )
@section('content')

<div class="row">
    <div class="col-md-6 col-xl-4 dash_board">
        <a href="{{route('delete.order')}}">
            <div class="card  widget-content  bg-midnight-bloom">
                <div class="widget-content-wrapper background_order">
                    <div class="widget-content-left font_dash">
                        <div class="widget-heading font_text opacity total_order">
                         Đơn hàng
                    </div>
                        <div class="widget-subheading opacity">
                            <i class="fa fa-cart-plus icon_order"></i>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </a>

    </div>

    <div class="col-md-6 col-xl-4 dash_board">
        <a href="{{route('delete.product')}}">
            <div class="card mb-3 widget-content bg-arielle-smile">
                <div class="widget-content-wrapper background_product">
                    <div class="widget-content-left font_dash">
                        <div class="widget-heading font_text opacity total_product">
                            Sản phẩm
                        </div>
                        <div class="widget-subheading opacity">
                            <i class="fa fa-archive icon_product"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>
    <div class="col-md-6 col-xl-4 dash_board">
        <a href="{{route('delete.category')}}">
            <div class="card mb-3 widget-content bg-grow-early">
                <div class="widget-content-wrapper background_post">
                    <div class="widget-content-left font_dash">
                        <div class="widget-heading font_text opacity total_post">
                            Danh mục
                        
                        </div>
                        <div class="widget-subheading opacity">
                            <i class="fa fas fa-th icon_post "></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>
</div>

@endsection
@section('scripts')
   

@endsection

