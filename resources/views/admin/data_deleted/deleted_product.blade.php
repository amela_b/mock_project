@extends('admin.layouts.app-new')
@section('title-page', 'Danh sách dữ liệu sản phẩm đã xóa')
@section('content')
@if (session('error'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-danger mg-b-0 " role="alert">
                                {{ session('error') }}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
                            <div class="alert alert-success mg-b-0 ">
                                {{session('success')}}
                                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
                            </div>
                        </div>
                    @endif
    <div class="d_flex res_end res_mr_tb">

        <div class="d_none khoiphuc mr20 cur_pointer">
            <p class=" text_c color_w gui_don">Khôi phục (<span class="soluong_chon"></span>)</p>

        </div>


        {{-- <div class="d_none xoavinhvien mr20 cur_pointer">
            <p class=" text_c color_red huy_btn">Xóa vĩnh viễn (<span class="soluong_chon"></span>)</p>
        </div> --}}

        <div class=" d_none bochontatca cur_pointer">
            <p class=" text_c color_blue">Bỏ chọn tất cả</p>
        </div>
        <div class=" bochon cur_pointer">
            <p class=" text_c color_blue">Bỏ chọn</p>
        </div>
        <div class="chontatca cur_pointer">
            <p class=" text_c color_blue ">Chọn tất cả</p>
        </div>
    </div>
    <div class="row">
        @foreach ($listProduct as $item)
            <div class="col-md-6 col-xl-4 dash_board">
                <div class="card  widget-content  bg-midnight-bloom">
                    <div class="widget-content-wrapper background_delete">
                        <div class="widget-content-left font_dash">
                            <div class="widget-heading font_text opacity total_order">
                                <i class="fa fa-cart-plus icon_order"></i>
                            </div>
                            <div class="widget-subheading opacity">
                                <p class="color_black">Tên sản phẩm: {{ $item['name'] }}</p>
                                <input type="hidden" name="id" value="{{ $item['id'] }}" class="id_order">
                                <p class="color_black">Giá: {{ number_format($item['price']) }} VNĐ</p>
                                <p class="color_black">Số lượng:{{$item['quantity'] }}</p>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        @endforeach


    </div>

@endsection
@section('scripts')
    <script>
        $('.chontatca').click(function() {
            $(this).hide();
            $('.background_delete').addClass('back_xoa');
            $('.khoiphuc,.xoavinhvien,.bochontatca').addClass('block');
            var count = 0;
            $('.back_xoa').each(function() {
                count++;
            });
            $('.soluong_chon').html(count);
        });
        $('.bochontatca').click(function() {
            $('.khoiphuc,.xoavinhvien,.bochontatca,.bochon').removeClass('block');
            $('.background_delete').removeClass('back_xoa');
            $('.chontatca').show();
        });
        $('.bochon').click(function() {
            $('.khoiphuc,.xoavinhvien,.bochon,.bochontatca').removeClass('block');
            $('.khoidulieu_daxoa_sp').removeClass('back_xoa');
            $('.chontatca').show();
        });
        $('.background_delete').click(function() {
            $(this).toggleClass('back_xoa');
            $('.khoiphuc,.xoavinhvien,.bochon').addClass('block');
            $('.chontatca').hide();
            $('.bochontatca').removeClass('block');
            var hClass = $('.background_delete').hasClass('back_xoa');
            if (!hClass) {
                $('.khoiphuc,.xoavinhvien,.bochon,.bochontatca').removeClass('block');
                $('.chontatca').show();
            }
            var count = 0;
            $('.back_xoa').each(function() {
                count++;
            });
            $('.soluong_chon').html(count);
        });
        $(document).on('click', '.khoiphuc', function() {
            var id = new Array();
            $('.back_xoa').each(function() {
                id.push($(this).find('.id_order').val());
               
            });

            $.ajax({
                url: "{{ route('restore_product') }}",
                method: 'GET',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },

                success: function(result) {
                    location.reload();

                },
                error: function() {
                    console.log('error');
                }
            });
        });
       
    </script>
@endsection
