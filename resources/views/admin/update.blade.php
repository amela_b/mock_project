@extends('admin.layouts.app-new')
@section('title-page', 'Cập nhật tài khoản' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('editUser',$user->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Họ tên</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                        <input name="name" placeholder="Nhập họ tên" value="{{$user->name}}" type="text" autocomplete="off"
                                                class="form-control" required>
                                            @if ($errors->has('name'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <label class="">Số điện thoại</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="text"  placeholder="Nhập SĐT" value="{{$user->phone}}" autocomplete="off"  title="Nhập SĐT" name="phone" class="form-control"  required>
                                                @if ($errors->has('phone'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('phone') }}</strong></span>
                                                @endif
                                            </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Địa chỉ</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập địa chỉ" value="{{$user->address}}" autocomplete="off"  title="Nhập địa chỉ" name="address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">email</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập email" value="{{$user->email}}" autocomplete="off"  title="Nhập email" name="email" class="form-control" >
                                            @if ($errors->has('email'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
