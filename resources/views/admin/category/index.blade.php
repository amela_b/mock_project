@extends('admin.layouts.app-new')
@section('title-page', 'Danh sách danh mục' )
@section('content')
{{-- <div class="container"> --}}
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="main-card card">
                <div class="card-body">
                    <form action="" id="search_lucky" method="get">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <input type="text" name="name" id="name" value="{{request('name')}}" placeholder="Tên danh mục" class="form-control">
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <button type="submit" class="btn btn-primary mr-2 search"><i class="fa fa-search"></i> Tìm kiếm</button>
                            </div>
                        </div>
                    </form>
                    <div class="row mt-2">
                        <div class="col-md-6 col-lg-6">
                            <h4>Tổng số danh mục: {{count($category)}}</h4>
                        </div>
                        <div class="col-md-6 col-lg-6 text-right">
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal1">
                                <i class="fa fa-plus"></i>
                                Thêm Danh mục cha
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form role="form" action="{{route('category.store')}}"  method="POST" id="create_category" enctype="multipart/form-data">
                {!! csrf_field() !!}
          <!-- Modal content-->
                <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                        <h4 class="modal-title">Thêm mới</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Tên danh mục</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập tên danh mục" value="" autocomplete="off"  
                                title="Nhập tên danh mục" name="name" class="form-control" required>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 mt-2">
                                <label class="">Ảnh danh mục</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 mt-2">
                                <input type="file" required accept="image/*"
                                                onchange="preview_image(event, 'image')" 
                                                name="image">
                                <img width="100px" class="mt-2"  id="image">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary send1">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
      <div id="myModal2" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form role="form" action=""  method="POST" id="update_category" enctype="multipart/form-data">
                {!! csrf_field() !!}
          <!-- Modal content-->
                <div class="modal-content" style="width:700px">
                    <div class="modal-header">
                        <h4 class="modal-title">Sửa danh mục</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mt-2">
                            <div class="col-lg-5 col-md-5 col-sm-5">
                                <label class="">Tên danh mục</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7">
                                <input type="text"  placeholder="Nhập tên danh mục"  name="name" id="name_category" class="form-control">
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 mt-2">
                                <label class="">Ảnh danh mục</label>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-7 mt-2">
                                <input type="file" accept="image/*"
                                    onchange="preview_image(event, 'image')" 
                                    name="image">
                                <img src="" width="100px" class="mt-2"  id="image_category">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <button type="submit" class="btn btn-primary send1">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Danh sách danh mục</h5>
                    <div class="table-responsive">
                        <table id="myTable" class="mb-0 table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên danh mục</th>
                                    <th>Ảnh</th>
                                    <th>Thao tác</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach ($category as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{ $item['name']}}</td>
                                        <td><img src="{{$item['image']}}" alt="" width="80"></td>
                                        <td>
                                            <button href="{{route('category.edit',$item['id'])}}" name="{{ $item['name']}}" 
                                            image="{{ $item['image']}}" type="button" class="btn btn-warning" 
                                            data-toggle="modal" data-target="#myModal2" onclick="return UpdateCategory(this);">Sửa</button>
                                            <a href="{{route('category.delete',$item['id'])}}" class="btn btn-danger delete_order">Xóa</a>
                                        </td>
                                    </tr> 
                                    @endforeach
                                
                            </tbody>
                        </table>
                        {!! $category->withQueryString()->links('pagination::bootstrap-5') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
{{-- </div> --}}
@endsection
@section('scripts')
    <script>
        function UpdateCategory(e) {
            var url = $(e).attr('href');
            var name = $(e).attr('name');
            var image = $(e).attr('image');
            console.log(url);
            $('#update_category').attr('action', url);
            $('#name_category').attr('value', name);
            $('#image_category').attr('src', image);
            return false;
        }
        $('.delete_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn xóa?");
            if (x)
                return true;
            else
                return false;
        });
        $('.duyet_order').click(function(){
            var x = confirm("Bạn có chắc chắn muốn duyệt?");
            if (x)
                return true;
            else
                return false;
        });
    </script>
    @endsection

