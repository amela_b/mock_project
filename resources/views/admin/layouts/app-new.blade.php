<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title-page')</title>
  <link rel="icon" href="{{asset('/upload/logo_title.png')}}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  {{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}"> --}}
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('AdminLTE-3.0.5/plugins/summernote/summernote-bs4.css')}}">
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
  <style>
       .select2-container .select2-selection--single {
          box-sizing: border-box;
          cursor: pointer;
          display: block;
          height: 48px;
          user-select: none;
          -webkit-user-select: none;
      }
      .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: rgb(89, 114, 226);
      }
      .nav-sidebar>.nav-item {
          margin-bottom: 15px;
      }
      .background_order{
        background: #e17272;
        height: 115px;
      }
      .background_delete{
        background: #fff;
        height: 115px;
      }
      .back_xoa{
        background: #aeaeae;
        height: 115px;
      }
      .color_black{
        color:#000;
        padding-left:15px;
      }
      .total_order{
        float: right;
        margin: 15px;
        font-size: 30px;
        color: #fff;
      }
      .quantity_order{
        font-size: 35px;
        text-align:center;
      }
      .icon_order{
        margin: 30px;
        font-size: 50px;
        color: #b6178c;
      }
      .background_product{
        background: #54b7dd;
        height: 115px;
      }
      .total_product{
        float: right;
        margin: 15px;
        font-size: 30px;
        color: #fff;
      }
      .quantity_product{
        font-size: 35px;
        text-align:center;
      }
      .icon_product{
        margin: 30px;
        font-size: 50px;
        color: #2a447e;
      }
      .background_post{
        background: #7e2a46;
        height: 115px;
      }
      .total_post{
        float: right;
        margin: 15px;
        font-size: 30px;
        color: #fff;
      }
      .quantity_post{
        font-size: 35px;
        text-align:center;
      }
      .icon_post{
        margin: 30px;
        font-size: 50px;
        color: #c8bb17;
      }
      input[type="date"]:before {
        color: lightgrey;
        content: attr(placeholder) !important;
        margin-right: 0.5em;
    }
    input[type="date"]:focus:before {
        content: '' !important;
    }
    input[type="date"].full:before {
        color:black;
        content:""!important;
    }
    .d_flex{
      display: flex;
    }
    .d_none{
      display: none;
    }
    .block{
      display: block;
    }
    .chontatca{
    width: 120px;
    height: 40px;
    background: #FFFFFF;
    border: 1px solid #4C5BD4;
    box-sizing: border-box;
    border-radius: 10px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px;
    margin-bottom: 15px;
}
.bochontatca{
    width: 120px;
    height: 40px;
    background: #FFFFFF;
    border: 1px solid #4C5BD4;
    box-sizing: border-box;
    border-radius: 10px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px;
    
}
.bochon{
    width: 120px;
    height: 40px;
    background: #FFFFFF;
    border: 1px solid #4C5BD4;
    box-sizing: border-box;
    border-radius: 10px;
    padding-top: 10px;
    font-weight: 700;
    font-size: 14px;
    display: none;
}
.gui_don{
    width: 120px;
    height: 40px;
    color: #fff;
    font-size: 14px;
    background: #4C5BD4;
    border-radius: 10px;
    padding-top: 10px;
    font-weight: 700;
}
.huy_btn{
    width: 120px;
    height: 40px;
    font-size: 14px;
    color: #FF3333;
    padding-top: 11px;
    font-weight: 700;
    border: 1px solid #FF3333;
    box-sizing: border-box;
    border-radius: 10px;
}
.khoiphuc,.xoavinhvien {
        margin-right: 5px;
    }
.cur_pointer{
    cursor: pointer; 
}
.color_blue {
    color: #4C5BD4 ;
}
.text_c {
    text-align: center;
}
   </style>
  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    @include('admin.layouts.nav-header')
    @include('admin.layouts.sidebar')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    {{-- header-content --}}
    @include('admin.layouts.header-content')
    {{-- header-content --}}

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if (session('error'))
        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
            <div class="alert alert-danger mg-b-0" role="alert" id="session_flash">
                {{ session('error') }}
                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
            </div>
        </div>
        <?php Session::forget('error')?>
        @endif
        @if (session('success'))
        <div class="col-12 col-xs-12 col-md-12 col-lg-12  pd-0 pd-t-15">
            <div class="alert alert-success mg-b-0" id="session_flash">
                {{session('success')}}
                <button type="button" class="close iconAlert" data-dismiss="alert" aria-label="Close">x</button>
            </div>
        </div>
        <?php Session::forget('success')?>
        @endif
        <!-- Small boxes (Stat box) -->
        @yield('content')
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('AdminLTE-3.0.5/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('AdminLTE-3.0.5/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
@yield('scripts')
<script>
  $(document).ready(function() {
            $('.select2').select2({width: '100%'});
          });
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('AdminLTE-3.0.5/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('AdminLTE-3.0.5/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('AdminLTE-3.0.5/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('AdminLTE-3.0.5/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('AdminLTE-3.0.5/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('AdminLTE-3.0.5/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('AdminLTE-3.0.5/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('AdminLTE-3.0.5/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('AdminLTE-3.0.5/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('AdminLTE-3.0.5/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-3.0.5/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('AdminLTE-3.0.5/dist/js/pages/dashboard.js')}}"></script>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-3.0.5/dist/js/demo.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
  CKEDITOR.replace('ckeditor');
        $(document).ready(function () {
            if($("#session_flash").hasClass("alert")){
                setTimeout(function(){
                    $('.alert').css('display',"none");
                }, 3000 );
            };
        });
</script>
</body>
</html>
