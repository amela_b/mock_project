@extends('admin.layouts.app-new')
@section('title-page', 'Thêm mới đơn hàng' )
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ">
                        <form action="{{route('order.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Tên Khách hàng</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input name="name" placeholder="Tên khách hàng" value="" type="text" autocomplete="off"
                                                class="form-control" required>
                                            @if ($errors->has('name'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <label class="">Số điện thoại</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8">
                                                <input type="text"  placeholder="Nhập SĐT" value="" autocomplete="off"  title="Nhập SĐT" name="phone" class="form-control"  required>
                                                @if ($errors->has('phone'))
                                                <span
                                                    class="help-block text-danger "><strong>{{ $errors->first('phone') }}</strong></span>
                                                @endif
                                            </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Địa chỉ</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập địa chỉ" value="" autocomplete="off"  title="Nhập địa chỉ" name="address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">email</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập email" value="" autocomplete="off"  title="Nhập email" name="email" class="form-control" >
                                            @if ($errors->has('email'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Sản phẩm</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                           
                                            <select multiple  class="form-control select2" id="product_id"
                                                name="product_id[]" >
                                                <option value="">--Chọn sản phẩm--</option>
                                                @foreach($product as $p)
                                                        <option value="{{$p->id}}"
                                                        >{{$p->name.'- Mã:'.$p->code}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <label class="">Số lượng</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                            <input type="text"  placeholder="Nhập Số lượng" value="" autocomplete="off"  title="Nhập số lượng" name="quantity" class="form-control" >
                                            @if ($errors->has('quantity'))
                                            <span
                                                class="help-block text-danger "><strong>{{ $errors->first('quantity') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="mt-2 btn btn-primary"> <i class="fa fa-check"></i> Lưu lại</button>
                                    <a href="{{route('order.index')}}" class="mt-2 btn btn-danger text-white"><i class="fa fa-arrow-circle-o-left"></i>
                                        Trở về</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
