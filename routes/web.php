<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Admin\DataDeleteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});
Route::post('/login', [HomeController::class, 'loginUser'])->name('login');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
// Route::get('/home', function () {
//     return view('admin.dashboard');
// });

Route::namespace ('Admin')->group(function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/home', [HomeController::class, 'index'])->name('home');
   

        Route::prefix('User')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('user.index');
            Route::get('/create', [UserController::class, 'create'])->name('user.create');
            Route::post('/store', [UserController::class, 'store'])->name('user.store');
            Route::get('/update/{id}', [UserController::class, 'update'])->name('user.update');
            Route::post('/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
            Route::get('/delete/{id}', [UserController::class, 'delete'])->name('user.delete');
            Route::get('/resetPassword/{id}', [UserController::class, 'resetPassword'])->name('user.resetPassword');
        });

        Route::prefix('Category')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('category.index');
            Route::get('/delete/{id}', [CategoryController::class, 'delete'])->name('category.delete');
            Route::post('/store', [CategoryController::class, 'store'])->name('category.store');
            Route::post('/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
        });

        Route::prefix('product')->group(function () {
            Route::get('/', [ProductController::class, 'index'])->name('product.index');
            Route::get('/create', [ProductController::class, 'create'])->name('product.create');
            Route::get('/update/{id}', [ProductController::class, 'update'])->name('product.update');
            Route::get('/delete/{id}', [ProductController::class, 'delete'])->name('product.delete');
            Route::post('/store', [ProductController::class, 'store'])->name('product.store');
            Route::post('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
            Route::get('/update_hot/{id}', [ProductController::class, 'update_hot'])->name('product.update_hot');
            Route::get('/update_sale/{id}', [ProductController::class, 'update_sale'])->name('product.update_sale');
        });

        Route::prefix('Order')->group(function () {
            Route::get('/', [OrderController::class, 'index'])->name('order.index');
            Route::get('/create', [OrderController::class, 'create'])->name('order.create');
            Route::get('/update/{id}', [OrderController::class, 'update'])->name('order.update');
            Route::get('/delete/{id}', [OrderController::class, 'delete'])->name('order.delete');
            Route::post('/store', [OrderController::class, 'store'])->name('order.store');
            Route::post('/edit/{id}', [OrderController::class, 'edit'])->name('order.edit');
            Route::get('/check_order/{id}', [OrderController::class, 'check_order'])->name('order.check_order');
        });

        Route::prefix('DataDelete')->group(function () {
            Route::get('/', [DataDeleteController::class, 'index'])->name('delete.index');
            Route::get('/DataDelete_order', [DataDeleteController::class, 'deletedOrder'])->name('delete.order');
            Route::get('/restore_order', [DataDeleteController::class, 'restore'])->name('restore_order');

            Route::get('/DataDelete_product', [DataDeleteController::class, 'deletedProduct'])->name('delete.product');
            Route::get('/restore_product', [DataDeleteController::class, 'restoreProduct'])->name('restore_product');

            Route::get('/DataDelete_category', [DataDeleteController::class, 'deletedCategory'])->name('delete.category');
            Route::get('/restore_category', [DataDeleteController::class, 'restoreCategory'])->name('restore_category');
           
        });

    });
});

require __DIR__ . '/auth.php';
