<?php

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\BannerController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\DataDeleteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware('api')->prefix('/')->group(function (){
    Route::post('/register', [UserController::class, 'register'])->name('register');
    Route::post('/login', [UserController::class, 'login'])->name('login');
    Route::post('/logout', [UserController::class, 'logout'])->name('logout');
    Route::put('/update-user', [UserController::class, 'edit'])->name('update-user');
    Route::get('/get-user-info', [UserController::class, 'getUserInfo'])->name('get-user-info');
    Route::put('/change-password', [UserController::class, 'changePassword'])->name('change-password');



    Route::get('/list-category', [CategoryController::class, 'listCategory'])->name('list-category');


    Route::get('/list-product', [ProductController::class, 'listProduct'])->name('list-product');
    Route::get('/list-sell-product', [ProductController::class, 'listSellProduct'])->name('list-sell-product');
    Route::get('/detail-product/{id}', [ProductController::class, 'detailProduct'])->name('detail-product');
    Route::post('/create-product', [ProductController::class, 'createProduct']);


    Route::get('/list-banner', [BannerController::class, 'listBanner'])->name('list-banner');
    Route::post('/create-banner', [BannerController::class, 'createBanner'])->name('create-banner');


    Route::get('/list-cart', [CartController::class, 'listCart'])->name('list-cart');
    Route::post('/add-to-cart', [CartController::class, 'createCart'])->name('add-to-cart');
    Route::patch('/update-cart/{id}', [CartController::class, 'editCart'])->name('update-cart');
    Route::delete('/delete-cart/{id}', [CartController::class, 'deleteCart'])->name('delete-cart');
    Route::post('/delete-cart-multiple', [CartController::class, 'deleteCartMultiple'])->name('delete-cart-multiple');



    Route::post('/add-order', [OrderController::class, 'storeOrder'])->name('add-order');
    Route::get('/list-order', [OrderController::class, 'listOrder'])->name('list-order');
    Route::delete('/cancel-order/{id}', [OrderController::class, 'cancelOrder'])->name('cancel-order');
    Route::post('/buy-again/{id}', [OrderController::class, 'buyAgain'])->name('buy-again');

});
